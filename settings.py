###
# Script by Ezechiel
# Date : 2019/05/26
# Description :  PiDimBoot settings 
###

import time, os

from const import *
from button import *
from database import *


class Singleton(type):
	_instances = {}
	def __call__(cls, *args, **kwargs):
		if cls not in cls._instances:
			cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
		return cls._instances[cls]



class Setting(object):
	__metaclass__ = Singleton
	
	sub_navigation = "SETTING"
	
	current_ip = 0
	pygame = None
	screen = None
	view_ping_success = False
	view_ping_fail = False
	
	ip0, ip1, ip2 = 0 , 0 , 0
	ip3, ip4, ip5 = 0 , 0 , 0
	ip6, ip7, ip8 = 0 , 0 , 0
	ip9, ip10, ip11 = 0 , 0 , 0
	
	
	time_interface = None #timestamps
	time_setting = None #timestamps
	change_value = False #False for View, True if user set a new IP Address in attent to save database or return
	
	def __init__(self):
		self.time_setting = time.time()
		
	@property
	def setTimestamp(self):
		self.time_interface = time.time()
		
	@property
	def setTimestampSettingInterface(self):
		self.time_setting = time.time()
	
	#
	def setSubNavigation(self, value):
		self.sub_navigation = value
		
	@property
	def getSubNavigation(self):
		return self.sub_navigation
	
		
	#Increment number max 9, restart 0
	def rotateIncrNumber(self, number):
		number += 1

		if number > 9:
			number = 0

		return number			
		
	#Decremente number min 0, restart 9
	def rotateDecrNumber(self, number):
		number -= 1

		if number < 0:
			number = 9

		return number
		
	#
	def createInterface(self, pygame, screen):
		self.pygame = pygame
		self.screen = screen
		img_bg = pygame.image.load(SETTING_IMG).convert()
		screen.blit(img_bg,(0,0))
		
		backButton(pygame, screen, BUTTON_BACK, GREY)
		
		#Home Setting Interface
		if self.sub_navigation == "SETTING":
			self.setTimestampSettingInterface
			
			fontLabel(pygame, screen, ARCADE_TTF, "SETTING", 40, 140, 2, WHITE)
			createButton(pygame, screen, BUTTON_BIG_BLUE, STARCRAFT_TTF, "Zero Key", 30, 160, 60 , WHITE)
			
			createButton(pygame, screen, BUTTON_BIG_BLUE, STARCRAFT_TTF, "", 30, 160, 130 , WHITE)
			logo = pygame.image.load(LOGO_NAOMI).convert_alpha()
			screen.blit(logo,(150,122))
			
			createButton(pygame, screen, BUTTON_BIG_BLUE, STARCRAFT_TTF, "", 28, 160, 200 , WHITE)
			logo = pygame.image.load(LOGO_CHIHIRO).convert_alpha()
			screen.blit(logo,(140,195))
			
			createButton(pygame, screen, BUTTON_BIG_BLUE, STARCRAFT_TTF, "", 30, 160, 270 , WHITE)
			logo = pygame.image.load(LOGO_TRIFORCE).convert_alpha()
			screen.blit(logo,(140,262))
			
			
		#Zero Key interface
		if self.sub_navigation == "ZERO":
			fontLabel(pygame, screen, ARCADE_TTF, "ZERO KEY", 40, 120, 2, BLACK)
			fontLabel(pygame, screen, SEGAZ_TTF, "Inject Zero Code :", 20, 20, 80, BLACK)
			
			if self.getZeroKey == 1:
				fontLabel(pygame, screen, SEGAZ_TTF, "ACTIVATE", 20, 300, 80, GREEN)
			else:
				fontLabel(pygame, screen, SEGAZ_TTF, "DEACTIVATE", 20, 300, 80, RED)
			
			
			createButton(pygame, screen, BUTTON_CHOICE_GREEN_SMALL, SEGAZ_TTF, "YES", 30, 110, 140 , WHITE, 25, 20)
			createButton(pygame, screen, BUTTON_CHOICE_ROSE_SMALL, SEGAZ_TTF, "NO", 30, 280, 140 , BLACK, 35, 20)
			
			
		#Naomi interface
		if self.sub_navigation == "NAOMI":
			fontLabel(pygame, screen, ARCADE_TTF, "NAOMI", 40, 120, 2, BLACK)
			fontLabel(pygame, screen, SEGAZ_TTF, "Set IP Address", 20, 20, 80, BLACK)
			
			self.current_ip = self.getIpSystem("ip_naomi")

			if not self.change_value:
				self.setTimestamp
				self.ip0, self.ip1, self.ip2 = self.current_ip[0:1], self.current_ip[1:2], self.current_ip[2:3]
				self.ip3, self.ip4, self.ip5 = self.current_ip[4:5], self.current_ip[5:6], self.current_ip[6:7]
				self.ip6, self.ip7, self.ip8 = self.current_ip[8:9], self.current_ip[9:10], self.current_ip[10:11]
				self.ip9, self.ip10, self.ip11 = self.current_ip[12:13], self.current_ip[13:14], self.current_ip[14:15]
			
			self.designIntefaceIpAddress(pygame, screen)
			
		#Chihiro interface
		if self.sub_navigation == "CHIHIRO":
			fontLabel(pygame, screen, ARCADE_TTF, "CHIHIRO", 40, 120, 2, BLACK)
			fontLabel(pygame, screen, SEGAZ_TTF, "Set IP Address", 20, 20, 80, BLACK)
			
			self.current_ip = self.getIpSystem("ip_chihiro")

			if not self.change_value:
				self.setTimestamp
				self.ip0, self.ip1, self.ip2 = self.current_ip[0:1], self.current_ip[1:2], self.current_ip[2:3]
				self.ip3, self.ip4, self.ip5 = self.current_ip[4:5], self.current_ip[5:6], self.current_ip[6:7]
				self.ip6, self.ip7, self.ip8 = self.current_ip[8:9], self.current_ip[9:10], self.current_ip[10:11]
				self.ip9, self.ip10, self.ip11 = self.current_ip[12:13], self.current_ip[13:14], self.current_ip[14:15]
			
			self.designIntefaceIpAddress(pygame, screen)
			
		#Triforce interface
		if self.sub_navigation == "TRIFORCE":
			fontLabel(pygame, screen, ARCADE_TTF, "TRIFORCE", 40, 120, 2, BLACK)
			fontLabel(pygame, screen, SEGAZ_TTF, "Set IP Address", 20, 20, 80, BLACK)
			
			self.current_ip = self.getIpSystem("ip_triforce")

			if not self.change_value:
				self.setTimestamp
				self.ip0, self.ip1, self.ip2 = self.current_ip[0:1], self.current_ip[1:2], self.current_ip[2:3]
				self.ip3, self.ip4, self.ip5 = self.current_ip[4:5], self.current_ip[5:6], self.current_ip[6:7]
				self.ip6, self.ip7, self.ip8 = self.current_ip[8:9], self.current_ip[9:10], self.current_ip[10:11]
				self.ip9, self.ip10, self.ip11 = self.current_ip[12:13], self.current_ip[13:14], self.current_ip[14:15]
			
			self.designIntefaceIpAddress(pygame, screen)
			
			
	#Get Ip in Database for System (Naomi, ..)
	def getIpSystem(self, system):
		database = Database()
		return database.getcursor.execute("SELECT %s FROM setting" % system).fetchone()[0]	
		
		
	#Set a new IP address
	def setNewIpAddress(self, system):
		new_ip = "%s%s%s.%s%s%s.%s%s%s.%s%s%s" % (self.ip0, self.ip1, self.ip2, self.ip3, self.ip4, \
		self.ip5, self.ip6, self.ip7, self.ip8, self.ip9, self.ip10, self.ip11)
		
		database = Database()
		database.getcursor.execute("UPDATE setting SET %s = '%s' WHERE id =1" % (system, new_ip))
		database.getconnexion.commit()
		
		self.change_value = False
		
		
	@property
	def formatViewIpAddress(self):
		cpt = 0
		string = ""
		
		for x in self.current_ip:
			if cpt == 0 :
				if x == '0':
					x = '-'
			
			if cpt == 1 :
				if x == '0' and string[cpt-1:cpt]== '-':
					x = '-'
					
			if cpt == 4 :
				if x == '0':
					x = '-'
				
			if cpt == 5 :
				if x == '0' and string[cpt-1:cpt] == '-':
					x = '-'
					
			if cpt == 8 :
				if x == '0':
					x = '-'
				
			if cpt == 9 :
				if x == '0' and string[cpt-1:cpt] == '-':
					x = '-'
					
			if cpt == 12 :
				if x == '0':
					x = '-'
			
			if cpt == 13 :
				if x == '0' and string[cpt-1:cpt] == '-':
					x = '-'
			
			string += x
			cpt += 1
				
		return string.replace("-", "")
		
		
	#Ping Ip Address
	@property
	def pingIdAddress(self):	
		fontLabel(self.pygame, self.screen, MEGADRIVE_TTF, "PING...", 70, 30, 140, RED)	
		self.pygame.display.flip()
		if os.system("ping -c 1 " + self.formatViewIpAddress) == 0:
			self.view_ping_success = True
		else:
			self.view_ping_fail = True
			 
	#Create IP Adress interface view
	def designIntefaceIpAddress(self, pygame, screen):
		#View Ip Address
		pos_x_button = [(self.ip0, 42, 24, 150), (self.ip1, 42, 54, 150), (self.ip2, 42, 94, 150), ('.', 30, 130, 160), \
		(self.ip3, 42, 144, 150), (self.ip4, 42, 174, 150), (self.ip5, 42, 208, 150), ('.', 30, 244, 160), \
		(self.ip6, 42, 254, 150), (self.ip7, 42, 284, 150), (self.ip8, 42, 314, 150), ('.', 30, 350, 160), \
		(self.ip9, 42, 364, 150), (self.ip10, 42, 394, 150), (self.ip11, 42, 424, 150),]
		
		for x in pos_x_button :				
			fontLabel(pygame, screen, SEGAZ_TTF, x[0], x[1], x[2], x[3], WHITE)
		
		#View button incremente and decremente
		pos_x_button = [(BUTTON_INCR, 24, 120), (BUTTON_DECR, 24, 200), \
		(BUTTON_INCR, 60, 120), (BUTTON_DECR, 60, 200), \
		(BUTTON_INCR, 96, 120), (BUTTON_DECR, 96, 200), \
		(BUTTON_INCR, 144, 120), (BUTTON_DECR, 144, 200), \
		(BUTTON_INCR, 178, 120), (BUTTON_DECR, 178, 200), \
		(BUTTON_INCR, 212, 120), (BUTTON_DECR, 212, 200), \
		(BUTTON_INCR, 254, 120), (BUTTON_DECR, 254, 200), \
		(BUTTON_INCR, 288, 120), (BUTTON_DECR, 288, 200), \
		(BUTTON_INCR, 320, 120), (BUTTON_DECR, 320, 200), \
		(BUTTON_INCR, 364, 120), (BUTTON_DECR, 364, 200), \
		(BUTTON_INCR, 395, 120), (BUTTON_DECR, 395, 200), \
		(BUTTON_INCR, 430, 120), (BUTTON_DECR, 430, 200),]
		
		for x in pos_x_button :				
			createButton(pygame, screen, x[0], None, "", 30, x[1], x[2] , WHITE, 0, 0)
		
		fontLabel(pygame, screen, None, self.formatViewIpAddress, 24, 145, 265, RED) #View Ip for PING
		
		if self.view_ping_success == True:
			fontLabel(self.pygame, self.screen, None, "SUCCESS", 24, 145, 285, GREEN)
	
		if self.view_ping_fail == True:
			fontLabel(self.pygame, self.screen, None, "Netdimm is unreachable!", 22, 140, 285, RED)
		
		createButton(pygame, screen, BUTTON_CHOICE_GREEN_SMALL, SEGAZ_TTF, "PING", 30, 35, 270 , WHITE, 20, 20)
		createButton(pygame, screen, BUTTON_CHOICE_ROSE_SMALL, SEGAZ_TTF, "SET", 30, 350, 270 , WHITE, 25, 20)
		
	
	#Protect interface for setting new Ip and not reload database value
	def setChangeValue(self, value):
		self.change_value = value
		
	#Re-init value to False
	@property
	def setFalsePingTestResult(self):
		self.view_ping_success = False
		self.view_ping_fail = False
		

	@property	
	def getZeroKey(self):
		database = Database()
		return database.getcursor.execute("SELECT zero_key FROM setting").fetchone()[0]
			
	#Define value Zero Key to database
	def setZeroKey(self, value):
		database = Database()
		database.getcursor.execute('''UPDATE setting SET zero_key = ? WHERE id =1''', [value])
		database.getconnexion.commit()