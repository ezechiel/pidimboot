###
# Script by Ezechiel
# Date : 2019/05/26
# Description :  PiDimBoot
###

import os, triforcetools, time
from gamelist import *
from database import *
from const import *
from button import *

class Singleton(type):
	_instances = {}
	def __call__(cls, *args, **kwargs):
		if cls not in cls._instances:
			cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
		return cls._instances[cls]
		
		
class System(object):
	__metaclass__ = Singleton
	
	sub_navigation = "All_System"
	
	pygame = None
	screen = None
	
	autoboot = False
	
	display_interface_game = False
	display_genre = False
	display_prevnext = False
	display_video = False
	covers_click = True
	
	count_system = 0	#number of system installed
	system = []
	current_game = ""	#name game on interface for start to netdimm
	index_system = 0
	index_game = 0
	
	display_by_genre = False
	sub_display_genre = False
	current_genre = ""
	index_genre = 0
	
	
	display_gallery = False
	gallery = []
	index_gallery = 0
	
	
	game_title = ""
	game_player = ""
	game_system = ""
	game_description = ""
	game_editor = ""
	game_genre = ""
	game_button = ""
	
	list_allGame_Atomisware = []
	list_allgenre_Atomisware = []
	dict_GenreGame_Atomisware = {}
	
	list_allGame_Naomi = []
	list_allgenre_Naomi = []
	dict_GenreGame_Naomi = {}
	
	list_allGame_Naomi2 = []
	list_allgenre_Naomi2 = []
	dict_GenreGame_Naomi2 = {}
	
	list_allGame_Triforce = []
	list_allgenre_Triforce = []
	dict_GenreGame_Triforce = {}
		
	list_allGame_Chihiro = []
	list_allgenre_Chihiro = []
	dict_GenreGame_Chihiro = {}
	database = None
	
	def __init__(self, pygame):
		self.database = Database()
		self.pygame = pygame
		
		#Atomisware all games
		tmp = []
		tmp = self.sortExistRomsSystem(ATOMISWAVE)
		self.list_allGame_Atomisware = self.lis_all_and_separate_game(tmp, self.list_allgenre_Atomisware, self.dict_GenreGame_Atomisware)

		
		#Naomi all games
		tmp = []
		tmp = self.sortExistRomsSystem(NAOMI1)
		self.list_allGame_Naomi = self.lis_all_and_separate_game(tmp, self.list_allgenre_Naomi, self.dict_GenreGame_Naomi)

		
		#Naomi2 all games
		tmp = []
		tmp = self.sortExistRomsSystem(NAOMI2)
		self.list_allGame_Naomi2 = self.lis_all_and_separate_game(tmp, self.list_allgenre_Naomi2, self.dict_GenreGame_Naomi2)

				
		#Triforce all games
		tmp = []
		tmp = self.sortExistRomsSystem(TRIFORCE)
		self.list_allGame_Triforce = self.lis_all_and_separate_game(tmp, self.list_allgenre_Triforce, self.dict_GenreGame_Triforce)

		
		#Chihiro all games
		tmp = []
		tmp = self.sortExistRomsSystem(CHIHIRO)
		self.list_allGame_Chihiro = self.lis_all_and_separate_game(tmp, self.list_allgenre_Chihiro, self.dict_GenreGame_Chihiro)
		
		tmp = None
		
		self.countSystemPresent
		if self.count_system > 0:
			self.system.sort()
	
		
	#Generate list games by system
	def sortExistRomsSystem(self, system):
		tmp = []
		list_game = []
		for type_game in GAME_LIST[system][GAMES]:
			for name_game, rom_game in GAME_LIST[system][GAMES][type_game].iteritems():
				tmp.append((name_game, rom_game, type_game))
			
		tmp.sort()
		
		for name_game, rom_game, type_game in tmp:
			list_game.append((rom_game, type_game))
		
		return self.testExistPhysicalRoms(list_game)
		
		
	#Delete/purge games of list not present in sd card
	def testExistPhysicalRoms(self, list_game):
		missing = []
		
		for value in list_game:
			if not os.path.isfile(ROM_DIR + value[0]):
				missing.append(value)
				
		for missing_game in missing:
			list_game.remove(missing_game)
			
		return list_game
		
	#Final list of game present in sdcard for all game, and list by genre
	def lis_all_and_separate_game(self, list_all_game, list_all_genre_available, list_all_game_by_genre):
		tmp = []
		list_game = []
		
		if len(list_all_game) > 0:
			for value in list_all_game:
				list_game.append(value[0])
				
				if value[1] not in tmp:
					tmp.append(value[1])
					list_all_genre_available.append(value[1])
					list_all_game_by_genre[value[1]] = []
			
				list_all_game_by_genre[value[1]].append(value[0])
				
			list_all_genre_available.sort()
				
		return list_game
		
		
	#Activate Virtualise Pic Zero Key
	@property
	def getPicZeroVirtualKey(self):
		respond = False
		
		if self.database.getcursor.execute("SELECT zero_key FROM setting").fetchone()[0] == 1:
			respond = True
		
		return respond
		
		
	#set Current Game
	def setCurrentGame(self, name_game):
		self.current_game = name_game
		
		
	#Add Game to Favorite Inteface
	@property
	def addFavorite(self):
		req = self.database.getcursor.execute('''SELECT * FROM favorites WHERE name_game = ?''', [self.current_game]).fetchone()
		
		if req is not None:
			self.delFavorite
		else:
			self.database.getcursor.execute('''INSERT INTO favorites (name_game) VALUES (?)''', [self.current_game])
			self.database.getconnexion.commit()
			
		
	@property	
	def delFavorite(self):
		self.database.getcursor.execute('''DELETE FROM favorites WHERE name_game = ?''', [self.current_game])
		self.database.getconnexion.commit()
		
	@property
	def getFavorite(self):
		favorite_game = False
		
		req = self.database.getcursor.execute('''SELECT * FROM favorites WHERE name_game = ?''', [self.current_game]).fetchone()
		
		if req is not None:
			favorite_game = True
			
		return favorite_game
		
		
		
	#Incremente or Decremente index for caroussel	
	@property	
	def incrIndexSystem(self):
		self.index_system += 1	
		
	@property
	def decrIndexSystem(self):
		self.index_system -= 1	
		
	@property	
	def incrIndexGame(self):
		self.index_game += 1	
		
	@property
	def decrIndexGame(self):
		self.index_game -= 1	
		
	@property	
	def incrIndexGenre(self):
		self.index_genre += 1	
		
	@property
	def decrIndexGenre(self):
		self.index_genre -= 1	
		
	@property	
	def incrIndexGallery(self):
		self.index_gallery += 1	
		
	@property
	def decrIndexGallery(self):
		self.index_gallery -= 1	
		
		
	@property
	def getDisplayInterfaceGame(self):
		return self.display_interface_game
		
	#Favorite Interface
	def createFavoriteInterface(self, screen):
		self.screen = screen
		self.display_interface_game = False
		self.sub_navigation = "Favorite"
		
		list_game = []
		
		req = self.database.getcursor.execute('''SELECT name_game \
		FROM games \
		WHERE name_game IN (SELECT name_game FROM favorites) \
		ORDER BY true_name ASC''').fetchall()
		
		for resultat in req:
			list_game.append(resultat[0])
			
		if len(list_game) == 0:
			img_bg = self.pygame.image.load(BACKGROUND_NOGAME).convert()
			self.screen.blit(img_bg,(0,0))
			backButton(self.pygame, self.screen, BUTTON_BACK_BLACK, GREY)
		else:
			self.getInterfaceAllGames(list_game)
		
		
				
	#Create Inteface System and allGames
	def createInterface(self, screen):
		self.screen = screen
		self.display_interface_game = False
		
		#No Game in SD Card
		if self.count_system == 0:
			img_bg = self.pygame.image.load(BACKGROUND_NOGAME).convert()
			self.screen.blit(img_bg,(0,0))
			backButton(self.pygame, self.screen, BUTTON_BACK_BLACK, GREY)
			
		#Game Present in SD CARD
		else:
			self.display_genre = False #No display button by genre
			
			if self.sub_navigation == 'All_System': 
				#Index for caroussel
				if self.index_system < 0:
					self.index_system = self.count_system - 1
					
				if self.index_system > self.count_system - 1:
					self.index_system = 0

				#Create Display System Interface
				if self.system[self.index_system] == "ATOMISWARE":
					self.getInterfaceSystems(BACKGROUND_ATOMISWAVE, self.list_allgenre_Atomisware)
					
				elif self.system[self.index_system] == "NAOMI":
					self.getInterfaceSystems(BACKGROUND_NAOMI, self.list_allgenre_Naomi)
					
				elif self.system[self.index_system] == "NAOMI2":
					self.getInterfaceSystems(BACKGROUND_NAOMI2, self.list_allgenre_Naomi2)
					
				elif self.system[self.index_system] == "TRIFORCE":
					self.getInterfaceSystems(BACKGROUND_TRIFORCE, self.list_allgenre_Triforce)
					
				elif self.system[self.index_system] == "CHIHIRO":
					self.getInterfaceSystems(BACKGROUND_CHIHIRO, self.list_allgenre_Chihiro)
					
				#Button enter to view game
				createButton(self.pygame, screen, BUTTON_METAL, SEGAZ_TTF, "ALL GAMES", 17, 350, 275 , WHITE, 8, 10)
					
				#if exist 2 differents systems
				if self.count_system > 1:
					prevButton(self.pygame, self.screen, BUTTON_PREV)
					nextButton(self.pygame, self.screen, BUTTON_NEXT)
					
				backButton(self.pygame, self.screen, BUTTON_BACK_BLACK, GREY)
					
			#Display Interface Games for System 
			elif self.sub_navigation == "all_ATOMISWARE" or self.sub_navigation == "all_NAOMI" \
			or self.sub_navigation == "all_NAOMI2" or self.sub_navigation == "all_TRIFORCE" \
			or self.sub_navigation == "all_CHIHIRO":
				self.getInterfaceAllGames()
				
			#Display Interface Genre by system
			elif self.sub_navigation == "genre_ATOMISWARE" or self.sub_navigation == "genre_NAOMI" \
			or self.sub_navigation == "genre_NAOMI2" or self.sub_navigation == "genre_TRIFORCE" \
			or self.sub_navigation == "genre_CHIHIRO":
				self.getInterfaceAllGenre()
				
				
	#Create display System Interface
	def getInterfaceSystems(self, img, list_genre):
		img_bg = self.pygame.image.load(img).convert()
		self.screen.blit(img_bg,(0,0))
		
		if len(list_genre) > 1 : #Only 2 genre differents
			self.display_genre = True
			createButton(self.pygame, self.screen, BUTTON_METAL_GREEN, SEGAZ_TTF, "BY GENRES", 17, 200, 275 , WHITE, 8, 10)

	
	#Create display Genre Interface
	def getInterfaceAllGenre(self):
		self.display_prevnext = False
		self.display_by_genre = True
		
		img_bg = self.pygame.image.load(BACKGROUND_ALL_GAMES).convert()
		self.screen.blit(img_bg,(0,0))
		
		backButton(self.pygame, self.screen, BUTTON_BACK, GREY)
		
		if not self.sub_display_genre:
			if self.sub_navigation == "genre_ATOMISWARE":
				self.getCarousselGenre(self.list_allgenre_Atomisware)	
				
			elif self.sub_navigation == "genre_NAOMI":
				self.getCarousselGenre(self.list_allgenre_Naomi)
							
			elif self.sub_navigation == "genre_NAOMI2":
				self.getCarousselGenre(self.list_allgenre_Naomi2)
				
			elif self.sub_navigation == "genre_TRIFORCE":
				self.getCarousselGenre(self.list_allgenre_Triforce)
							
			elif self.sub_navigation == "genre_CHIHIRO":
				self.getCarousselGenre(self.list_allgenre_Chihiro)
		else :
			list_game = []
			if self.sub_navigation == "genre_ATOMISWARE":
				self.getInterfaceAllGames(self.dict_GenreGame_Atomisware[self.current_genre])	
				
			elif self.sub_navigation == "genre_NAOMI":
				self.getInterfaceAllGames(self.dict_GenreGame_Naomi[self.current_genre])
							
			elif self.sub_navigation == "genre_NAOMI2":
				self.getInterfaceAllGames(self.dict_GenreGame_Naomi2[self.current_genre])
				
			elif self.sub_navigation == "genre_TRIFORCE":
				self.getInterfaceAllGames(self.dict_GenreGame_Triforce[self.current_genre])
							
			elif self.sub_navigation == "genre_CHIHIRO":
				self.getInterfaceAllGames(self.dict_GenreGame_Chihiro[self.current_genre])
				
		
		
	#Create display Game Intefarce
	def getInterfaceAllGames(self, list_game=None):
		self.display_prevnext = False
		img_bg = self.pygame.image.load(BACKGROUND_ALL_GAMES).convert()
		self.screen.blit(img_bg,(0,0))
		
		backButton(self.pygame, self.screen, BUTTON_BACK, GREY)
		
		if self.sub_navigation == "all_ATOMISWARE":
			self.getCarousselGames(self.list_allGame_Atomisware)	
			
		elif self.sub_navigation == "all_NAOMI":
			self.getCarousselGames(self.list_allGame_Naomi)
						
		elif self.sub_navigation == "all_NAOMI2":
			self.getCarousselGames(self.list_allGame_Naomi2)
			
		elif self.sub_navigation == "all_TRIFORCE":
			self.getCarousselGames(self.list_allGame_Triforce)
						
		elif self.sub_navigation == "all_CHIHIRO":
			self.getCarousselGames(self.list_allGame_Chihiro)
			
		elif self.sub_navigation == "Favorite" or self.sub_display_genre == True:
			self.getCarousselGames(list_game)
		
		
	#Display Genre on Caroussel
	def getCarousselGenre(self, list_genre):
		#Display all genre
		count_genre = len(list_genre)
		
		if count_genre > 1:
			self.display_prevnext = True
			prevButton(self.pygame, self.screen, BUTTON_PREV)
			nextButton(self.pygame, self.screen, BUTTON_NEXT)
			
			#Index caroussel
			if self.index_genre < 0:
				self.index_genre = count_genre - 1
				
			if self.index_genre > count_genre - 1:
				self.index_genre = 0
				
			self.current_genre = list_genre[self.index_genre]
			self.blit_text(self.current_genre, (100,130), self.pygame.font.Font(STARCRAFT_TTF, 40), WHITE)
			
			createButton(self.pygame, self.screen, BUTTON_METAL, SEGAZ_TTF, "GAMES", 17, 350, 275 , WHITE, 30, 10)
			
			
						
	#Display Game on caroussel	
	def getCarousselGames(self, list_games):
		if not self.display_gallery:
			#Display all game
			count_games = len(list_games)
			self.display_interface_game = True
			
			#if exist 2 differents games
			if count_games > 1:
				self.display_prevnext = True
				prevButton(self.pygame, self.screen, BUTTON_PREV)
				nextButton(self.pygame, self.screen, BUTTON_NEXT)
				
				#Index caroussel
				if self.index_game < 0:
					self.index_game = count_games - 1
					
				if self.index_game > count_games - 1:
					self.index_game = 0
					
			self.current_game = list_games[self.index_game]
			
			#covers
			self.viewCoversGames
			
			#Game Info
			self.viewInfomation
			fontLabel(self.pygame, self.screen, STARCRAFT_TTF, self.game_title, 16, 55, 170, WHITE)	
			
			fontLabel(self.pygame, self.screen, None, "Description :", 16, 20, 200, WHITE)
			self.blit_text(self.game_description, (90,200), self.pygame.font.SysFont(None, 16), WHITE)
			
			fontLabel(self.pygame, self.screen, None, "Genre :", 16, 20, 230, WHITE)
			fontLabel(self.pygame, self.screen, None, self.game_genre, 16, 60, 230, WHITE)
		
			fontLabel(self.pygame, self.screen, None, "Players :", 16, 160, 230, WHITE)
			fontLabel(self.pygame, self.screen, None, self.game_player, 16, 210, 230, WHITE)
			
			fontLabel(self.pygame, self.screen, None, "Editor :", 16, 290, 230, WHITE)
			fontLabel(self.pygame, self.screen, None, self.game_editor, 16, 340, 230, WHITE)
			
			fontLabel(self.pygame, self.screen, None, "System :", 16, 20, 260, WHITE)
			fontLabel(self.pygame, self.screen, None, self.game_system, 16, 70, 260, WHITE)
			
			fontLabel(self.pygame, self.screen, None, "Buttons :", 16, 140, 260, WHITE)
			fontLabel(self.pygame, self.screen, None, self.game_button, 16, 195, 260, WHITE)
			
			#button
			if self.sub_navigation != "Favorite":
				if self.getFavorite:
					createButton(self.pygame, self.screen, BUTTON_FAVORITE_ON, None, "", 17, 275, 275 , WHITE, 0, 0)
				else:
					createButton(self.pygame, self.screen, BUTTON_FAVORITE_OFF, None, "", 17, 275, 275 , WHITE, 0, 0)
			
			createButton(self.pygame, self.screen, BUTTON_METAL_GREEN, SEGAZ_TTF, "Send ", 17, 370, 285 , WHITE, 40, 10)
		else:
			#Display Gallery
			self.getGallery
			
		
	#return pictures gallery from game
	@property
	def getGallery(self):
		if os.path.isdir(IMAGE_GAME_DIR + self.current_game[:-4]):
			self.display_gallery = True
			self.gallery = []
			
			for pictures in os.listdir(IMAGE_GAME_DIR + self.current_game[:-4]):
				self.gallery.append(pictures)
		
			self.gallery.sort()
			count_gallery = len(self.gallery)
			
			#Index caroussel
			if self.index_gallery < 0:
				self.index_gallery = count_gallery - 1
				
			if self.index_gallery > count_gallery - 1:
				self.index_gallery = 0
			
			img = ('%s%s/%s') % (IMAGE_GAME_DIR, self.current_game[:-4], self.gallery[self.index_gallery])
			
			img_bg = self.pygame.image.load(img).convert()
			self.screen.blit(img_bg,(0,0))
			
			backButton(self.pygame, self.screen, BUTTON_BACK_NO_TRANSPARENT, GREY)
			
			if count_gallery > 1:
				prevButton(self.pygame, self.screen, BUTTON_PREV)
				nextButton(self.pygame, self.screen, BUTTON_NEXT)		
		
		
	#Auto return line
	def blit_text(self, text, pos, font, color):
		x, y = pos
		
		words = [word.split(' ') for word in text.splitlines()] 
		space = font.size(' ')[0] 
		max_width, max_height = self.screen.get_size()
		
		for line in words:
			for word in line:
				word_surface = font.render(word, 0, color)
				word_width, word_height = word_surface.get_size()
				
				if x + word_width >= max_width:
					x = pos[0]  # Reset the x.
					y += word_height  # Start on new row.
					
				self.screen.blit(word_surface, (x, y))
				x += word_width + space
				
			x = pos[0]  # Reset the x.
			y += word_height  # Start on new row.
		
		
	#Create view Information
	@property
	def viewInfomation(self):
		req = self.database.getcursor.execute('''SELECT * FROM games WHERE name_game = ?''', [self.current_game]).fetchone()
		
		if req is not None:
			self.game_title = req[2]
			self.game_player = req[3]
			self.game_system = req[4]
			self.game_description = req[5]
			self.game_editor = req[6]
			self.game_genre = req[7]
			self.game_button = req[8]
		else: 
			self.game_title = "No Information"
			self.game_player = "No Information"
			self.game_system = "No Information"
			self.game_description = "No Information"
			self.game_editor = "No Information"
			self.game_genre = "No Information"
			self.game_button = "No Information"
			
	
	#create covers games
	@property
	def viewCoversGames(self):
		self.covers_click = True
		img_location = "%s%s/%s" % (IMAGE_GAME_DIR, self.current_game[:-4], "covers.jpg")
		
		#test with PNG
		if not os.path.isfile(img_location):	
			img_location = "%s%s/%s" % (IMAGE_GAME_DIR, self.current_game[:-4], "covers.png")
		
		#No Covers, too bad..
		if not os.path.isfile(img_location):	
			self.covers_click = False	
			img_location = "%s%s" % (IMAGE_GAME_DIR, "no_covers.jpg")
		
		picture = self.pygame.image.load(img_location).convert()
		picture = self.pygame.transform.scale(picture, (200, 150))
		self.screen.blit(picture,(140,10))
		
		if self.covers_click:
			self.pygame.draw.rect(self.screen, GREY, (140, 10, 200, 150), 3)
		else:
			self.pygame.draw.rect(self.screen, RED, (140, 10, 200, 150), 3)
		
		
	#Re-Initialize index game for new display after back page		
	@property
	def reinitCurrentGenre(self):
		self.index_genre = 0
		
	#Re-Initialize index game for new display after back page		
	@property
	def reinitCurrentGame(self):
		self.index_game = 0
		
		
	#Count System installed in dir roms 
	@property	
	def countSystemPresent(self):
		if len(self.list_allGame_Atomisware) > 0:
			self.count_system  += 1
			self.system.append("ATOMISWARE")
		
		if len(self.list_allGame_Naomi) > 0:
			self.count_system  += 1
			self.system.append("NAOMI")
			
		if len(self.list_allGame_Naomi2) > 0:
			self.count_system  += 1
			self.system.append("NAOMI2")
			
		if len(self.list_allGame_Triforce) > 0:
			self.count_system  += 1
			self.system.append("TRIFORCE")
			
		if len(self.list_allGame_Chihiro) > 0:
			self.count_system  += 1
			self.system.append("CHIHIRO")
			
			
	#Return current page system		
	@property 
	def getCurrentSystem(self):
		return self.system[self.index_system]
	
			
	@property			
	def getCountAllSystem(self):
		return self.count_system
		
		
	#Set navigation page
	def setSubNavigation(self, value):
		self.sub_navigation = value
		
	#Get navigation page	
	@property
	def getSubNavigation(self):
		return self.sub_navigation
		
		
	@property
	def getDisplayByGenre(self):
		return self.display_genre
		
		
	@property
	def getDisplayNextPrev(self):
		return self.display_prevnext
		
		
	#Define True or False value for autoboot
	def setAutoboot(self, value):
		self.autoboot = value
		
		
	#Send Game to Netdimm
	@property
	def sendingGame(self):
		if not self.autoboot:
			fontLabel(self.pygame, self.screen, MEGADRIVE_TTF, "SENDING...", 40, 20, 140, RED)	
			self.pygame.display.flip()
			
		ip_value =(self.database.getcursor.execute("SELECT ip_naomi FROM setting").fetchone()[0], \
			self.database.getcursor.execute("SELECT ip_triforce FROM setting").fetchone()[0], \
			self.database.getcursor.execute("SELECT ip_chihiro FROM setting").fetchone()[0]
		)
		
		ip = ""
		
		#Ip Address
		if self.getDisplayInterfaceGame or self.autoboot :
			req = self.database.getcursor.execute('''SELECT system FROM games WHERE name_game = ?''', [self.current_game]).fetchone()[0]

			if req == "Naomi" or req == "Naomi 2" or req == "Atomiswave":
				ip = ip_value[0]
			elif req == "Triforce":
				ip = ip_value[1]
			elif req == "Chihiro":
				ip = ip_value[2]
		else: 	
			if self.getCurrentSystem == "NAOMI" or self.getCurrentSystem == "NAOMI2" or self.getCurrentSystem == "ATOMISWARE":
				ip = ip_value[0]
				
			elif self.getCurrentSystem == "TRIFORCE" or req == "":
				ip = ip_value[1]
				
			elif self.getCurrentSystem == "CHIHIRO":
				ip = ip_value[2]	
		
		if len(ip) > 0:
			if os.system("ping -c 1 %s" % ip) == 0:
				triforcetools.connect(ip, 10703)
				triforcetools.HOST_SetMode(0, 1)
				triforcetools.SECURITY_SetKeycode("\x00" * 8)
				triforcetools.DIMM_UploadFile(ROM_DIR + self.current_game)
				triforcetools.HOST_Restart()
				triforcetools.TIME_SetLimit(10*60*1000)
				
				if self.getPicZeroVirtualKey:
					time.sleep(1)
					
					while not self.pygame.event.get():					
						triforcetools.TIME_SetLimit(10*60*1000)
						time.sleep(1)
		
				triforcetools.disconnect()
			else:
				if not self.autoboot:
					fontLabel(self.pygame, self.screen, MEGADRIVE_TTF, "FAIL..", 40, 20, 200, RED)	
					self.pygame.display.flip()
					time.sleep(2)
