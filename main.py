###
# Script by Ezechiel
# Date : 2019/05/26
# Description :  PiDimBoot
###

import pygame, os
from pygame.locals import *
from time import sleep

from const import *
from button import *
from interaction import *
from settings import *
from database import *
from system import *


#Setup touchscreen
os.environ['SDL_VIDEODRIVER']= 'fbcon'
os.environ['SDL_FBDEV']= '/dev/fb1'
os.environ['SDL_MOUSEDRV'] = 'TSLIB'
os.environ["SDL_MOUSEDEV"] = '/dev/input/touchscreen'

#load database
database = Database()

#start pygame
pygame.init()
pygame.mouse.set_visible(False)
screen = pygame.display.set_mode((480,320))

img_bg = pygame.image.load(MAIN_IMG).convert() #background img loaded
navigation = 'HOME' #start nav menu

#Autoboot Rom
autoboot = False
file_autoboot = open(AUTOBOOT_DIR, "r")
value_autoboot = file_autoboot.readline()
value_autoboot = value_autoboot.replace(' ', '')
value_autoboot = value_autoboot.replace('\n', '')
file_autoboot.close

if len(value_autoboot) > 3:
	if os.path.isfile(ROM_DIR + value_autoboot):
		autoboot = True

while True:
	if autoboot:
		img_tmp = pygame.image.load(BACKGROUND_WHITE).convert()
		screen.blit(img_tmp, (0,0))
		
		fontLabel(pygame, screen, STARCRAFT_TTF, "Autoboot", 50, 35, 120, RED)
		fontLabel(pygame, screen, STARCRAFT_TTF, "Wait don't touch screen", 20, 15, 200, RED)
		pygame.display.flip() #refresh screen
		
		system = System(pygame)
		
		sleep(40) #Wait Naomi Startup
		
		screen.blit(img_bg, (0,0))
		pygame.display.flip() #refresh screen
		
		system.setCurrentGame(value_autoboot)	
		system.setAutoboot(True)
		system.sendingGame
		system.setAutoboot(False)
		
		sleep(2)
	else :
		navigation = Interaction().interaction(navigation, pygame)
		
		#homepage interface
		if navigation == 'HOME':
			screen.blit(img_bg,(0,0)) #apply background

			createButton(pygame, screen, BUTTON_BIG_BLUE, STARCRAFT_TTF, "SYSTEM", 30, 160, 130 , WHITE)
			createButton(pygame, screen, BUTTON_BIG_BLUE, STARCRAFT_TTF, "FAVORITE", 28, 160, 200 , WHITE)
			createButton(pygame, screen, BUTTON_BIG_BLUE, STARCRAFT_TTF, "SETTING", 30, 160, 270 , WHITE)
			createButton(pygame, screen, BUTTON_POWEROFF, STARCRAFT_TTF, "", 30, 410, 255 , WHITE, 0, 0)
			
		#System interface
		if navigation == 'SYSTEM':
			system = System(pygame)
			system.createInterface(screen)
			
		#Favorite interface
		if navigation == 'FAVORITE':
			system = System(pygame)
			system.createFavoriteInterface(screen)
			
		#setting interface
		if navigation == 'SETTINGS':
			setting = Setting()
			setting.createInterface(pygame, screen)
			
			
		pygame.display.flip() #refresh screen
	
	if not autoboot:
		pygame.event.wait() 
	else:
		autoboot = False
		
	sleep(0.2)
