###
# Script by Ezechiel
# Date : 2019/05/26
# Description :  PiDimBoot constantes
###


#Dir
ROM_DIR = '/boot/pidimboot/roms/'
VIDEO_DIR = '/boot/pidimboot/video/'
IMAGE_DIR = '/boot/pidimboot/img/'
IMAGE_GAME_DIR = '/opt/pidimboot/img/games/'
AUTOBOOT_DIR = '/boot/pidimboot/autoboot.txt'

#Database
DATABASE = '/opt/pidimboot/database/pidimboot.db'

#Policies
ARCADE_TTF = '/opt/pidimboot/fonts/Arcade.ttf'
STARCRAFT_TTF = '/opt/pidimboot/fonts/Starcraft.ttf'
MEGADRIVE_TTF = '/opt/pidimboot/fonts/Megadrive.ttf'
SEGAZ_TTF = '/opt/pidimboot/fonts/Segaz.ttf'


#Color
BLACK = 0, 0, 0
WHITE = 255, 255, 255
GREY = 197, 200, 204
RED = 240, 0, 0
GREEN = 19, 244, 7


#Backgroung pictures
MAIN_IMG = '/opt/pidimboot/img/src/main.jpg'
SETTING_IMG = '/opt/pidimboot/img/src/setting.jpg'
LOGO_NAOMI = '/opt/pidimboot/img/src/naomi_small_logo.png'
LOGO_CHIHIRO = '/opt/pidimboot/img/src/chihiro_small_logo.png'
LOGO_TRIFORCE = '/opt/pidimboot/img/src/Triforce_small_logo.png'
BACKGROUND_WHITE = '/opt/pidimboot/img/src/background_white.jpg'
BACKGROUND_NOGAME = '/opt/pidimboot/img/src/background_no_game.jpg'
BACKGROUND_ATOMISWAVE = '/opt/pidimboot/img/src/background_atomiswave.jpg'
BACKGROUND_NAOMI = '/opt/pidimboot/img/src/background_naomi.jpg'
BACKGROUND_NAOMI2 = '/opt/pidimboot/img/src/background_naomi2.jpg'
BACKGROUND_TRIFORCE = '/opt/pidimboot/img/src/background_triforce.jpg'
BACKGROUND_CHIHIRO = '/opt/pidimboot/img/src/background_chihiro.jpg'
BACKGROUND_ALL_GAMES = '/opt/pidimboot/img/src/background_allgames.jpg'



#Button
BUTTON_BIG_BLUE = '/opt/pidimboot/img/src/button.png'
BUTTON_BACK = '/opt/pidimboot/img/src/back.png'
BUTTON_BACK_BLACK = '/opt/pidimboot/img/src/back_black.png'
BUTTON_BACK_NO_TRANSPARENT = '/opt/pidimboot/img/src/back_noTransparent.jpg'
BUTTON_POWEROFF = '/opt/pidimboot/img/src/poweroff.png'
BUTTON_CHOICE_GREEN_SMALL = '/opt/pidimboot/img/src/choice_green.png'
BUTTON_CHOICE_ROSE_SMALL = '/opt/pidimboot/img/src/choice_rose.png'
BUTTON_DECR = '/opt/pidimboot/img/src/button_decr.png'
BUTTON_INCR = '/opt/pidimboot/img/src/button_incr.png'
BUTTON_PREV = '/opt/pidimboot/img/src/prev.png'
BUTTON_NEXT = '/opt/pidimboot/img/src/next.png'
BUTTON_FAVORITE_OFF = '/opt/pidimboot/img/src/button_favorite_off.png'
BUTTON_FAVORITE_ON = '/opt/pidimboot/img/src/button_favorite_on.png'
BUTTON_METAL = '/opt/pidimboot/img/src/button_metal.png'
BUTTON_METAL_GREEN = '/opt/pidimboot/img/src/button_metal_green.png'


#System
ATOMISWAVE = "ATOMISWAVE"
NAOMI1 = "NAOMI 1"
NAOMI2 = "NAOMI 2"
CHIHIRO = "CHIHIRO"
TRIFORCE = "TRIFORCE"

GAMES = "GAMES"
RACING = "RACING"
SHOOTER = "SHOOTER"
ACTION = "ACTION"
SPORT = "SPORT"
FIGHTING = "FIGHTING"
HORI_SHOOTEMUP = "HORIZONTAL SHOOTEMUP"
VERT_SHOOTEMUP = "VERTICAL SHOOTEMUP"
VARIOUS = "VARIOUS"
PUZZLE = "PUZZLE"
