###
# Script by Ezechiel
# Date : 2019/05/26
# Description :  PiDimBoot interaction 
###

import os , time, subprocess
from settings import *
from system import *

class Singleton(type):
	_instances = {}
	def __call__(cls, *args, **kwargs):
		if cls not in cls._instances:
			cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
		return cls._instances[cls]


class Interaction(object):
	__metaclass__ = Singleton
	
	block_action = False
	
	def __init__(self):
			pass

	def interaction(self, navigation, pygame):
		for event in pygame.event.get():
			if event.type == pygame.MOUSEBUTTONDOWN:
#				print((pygame.mouse.get_pos() [0], pygame.mouse.get_pos() [1]))
				touch_pos = (pygame.mouse.get_pos() [0], pygame.mouse.get_pos() [1])
				
				if navigation == 'HOME':				
					if 100 <= touch_pos[0] <= 380 and 115 <= touch_pos[1] <= 170:
						navigation = 'SYSTEM'
					
					if 100 <= touch_pos[0] <= 380 and 187 <= touch_pos[1] <= 240:
						navigation = 'FAVORITE'
						
					if 100 <= touch_pos[0] <= 380 and 260 <= touch_pos[1] <= 310:
						navigation = 'SETTINGS'
						
					if 408 <= touch_pos[0] <= 470 and 260 <= touch_pos[1] <= 310:
						os.system('sudo shutdown now')
				
				#Navigation menu System
				if navigation == 'SYSTEM' or navigation == 'FAVORITE':
					navigation = self.navigationSystem(navigation, touch_pos, pygame)
										
				#Navigation menu Setting
				if navigation == 'SETTINGS':		
					setting = Setting()
					
					#Back Button interaction		
					if 0 <= touch_pos[0] <= 50 and 0 <= touch_pos[1] <= 40:
						setting.setChangeValue(False)
						setting.setFalsePingTestResult
						setting.time_interface = None
						setting.time_setting = None
						
											
						if setting.getSubNavigation == 'SETTING':
							navigation = 'HOME'
							
						if setting.getSubNavigation == 'ZERO' or setting.getSubNavigation == 'NAOMI' or setting.getSubNavigation == 'CHIHIRO' or setting.getSubNavigation == 'TRIFORCE':
							setting.setSubNavigation('SETTING')
							navigation = 'SETTINGS'
					
					#Into Setting interface
					if setting.getSubNavigation == 'SETTING':	
						if setting.time_setting is not None:
							if time.time() - setting.time_setting >= 0.5:
								if 90 <= touch_pos[0] <= 385 and 45 <= touch_pos[1] <= 105:
									setting.setSubNavigation('ZERO')
								if 90 <= touch_pos[0] <= 385 and 118 <= touch_pos[1] <= 170:
									setting.setSubNavigation('NAOMI')
								if 90 <= touch_pos[0] <= 385 and 190 <= touch_pos[1] <= 240:
									setting.setSubNavigation('CHIHIRO')
								if 90 <= touch_pos[0] <= 385 and 257 <= touch_pos[1] <= 310:
									setting.setSubNavigation('TRIFORCE')
						
						
					#Into Zero Key interface	
					if setting.getSubNavigation == 'ZERO':
						if 85 <= touch_pos[0] <= 203 and 135 <= touch_pos[1] <= 175:
							setting.setZeroKey(1) #Active
							
						if 245 <= touch_pos[0] <= 365 and 135 <= touch_pos[1] <= 175:
							setting.setZeroKey(0) #deactivate
							
				
					#Into Naomi IP Interface
					if setting.getSubNavigation== 'NAOMI':
						if setting.time_interface is not None:
							if time.time() - setting.time_interface >= 0.5:
								self.interactionIpAdress(setting, touch_pos, "ip_naomi")
								
					#Into Chihiro IP Interface
					if setting.getSubNavigation== 'CHIHIRO':
						if setting.time_interface is not None:
							if time.time() - setting.time_interface >= 0.5:
								self.interactionIpAdress(setting, touch_pos, "ip_chihiro")
								
					#Into Triforce IP Interface
					if setting.getSubNavigation== 'TRIFORCE':
						if setting.time_interface is not None:
							if time.time() - setting.time_interface >= 0.5:
								self.interactionIpAdress(setting, touch_pos, "ip_triforce")
													
		return navigation
		
		
	def navigationSystem(self, navigation, touch_pos, pygame):
		system = System(pygame)
		
		if system.getSubNavigation == 'All_System' and system.display_gallery == True:
			#bug fixed, 
			system.display_gallery = False
	
		#Back Button interaction		
		if 0 <= touch_pos[0] <= 50 and 0 <= touch_pos[1] <= 40:	
			
			if not system.display_gallery:
				if system.getSubNavigation == 'All_System':
					navigation = 'HOME' 

				elif system.getSubNavigation == "all_ATOMISWARE" or system.getSubNavigation == "all_NAOMI" \
				or system.getSubNavigation == "all_NAOMI2" or system.getSubNavigation == "all_TRIFORCE" \
				or system.getSubNavigation == "all_CHIHIRO":
					system.reinitCurrentGame
					system.setSubNavigation('All_System')
					
				elif system.getSubNavigation == "genre_ATOMISWARE" or system.getSubNavigation == "genre_NAOMI" \
					or system.getSubNavigation == "genre_NAOMI2" or system.getSubNavigation == "genre_TRIFORCE" \
					or system.getSubNavigation == "genre_CHIHIRO":
					if not system.sub_display_genre:
						#First Level	
						system.reinitCurrentGenre
						system.display_by_genre = False
						system.setSubNavigation('All_System')
					else:
						#Second Level
						system.reinitCurrentGame
						system.sub_display_genre = False
					
				elif system.getSubNavigation == 'Favorite':
					system.reinitCurrentGame
					system.setSubNavigation('All_System')
					navigation = 'HOME' 
					
			else:
				system.display_gallery = False
				system.index_gallery = 0
	
	
		if system.getSubNavigation == 'All_System':
			if system.getCountAllSystem > 1:
				#Prev Button
				if 5 <= touch_pos[0] <= 45 and 140 <= touch_pos[1] <= 180:
					system.decrIndexSystem
					
				#Next Button	
				if 435 <= touch_pos[0] <= 475 and 140 <= touch_pos[1] <= 180:
					system.incrIndexSystem

						
		#By Genre
		if system.getDisplayByGenre:
			if 195 <= touch_pos[0] <= 320 and 265 <= touch_pos[1] <= 305:
				system.setSubNavigation("genre_%s" % system.getCurrentSystem)
		
		#All Games
		if not system.display_by_genre:
			if 340 <= touch_pos[0] <= 470 and 265 <= touch_pos[1] <= 305:
				system.setSubNavigation("all_%s" % system.getCurrentSystem)
		else:
			#All Genre
			if 320 <= touch_pos[0] <= 450 and 265 <= touch_pos[1] <= 305:
				system.sub_display_genre = True
			
		#Interface Game button Favorite and Send
		if system.getDisplayInterfaceGame:
			if 140 <= touch_pos[0] <= 340 and 10 <= touch_pos[1] <= 160:
				system.getGallery
				
			if 275 <= touch_pos[0] <= 313 and 275 <= touch_pos[1] <= 315:
				system.addFavorite
				
			if 330 <= touch_pos[0] <= 457 and 275 <= touch_pos[1] <= 315:
				system.sendingGame
		
			
		#Prev Next Games	
		if system.getSubNavigation == "all_ATOMISWARE" or system.getSubNavigation == "all_NAOMI" \
		or system.getSubNavigation == "all_NAOMI2" or system.getSubNavigation == "all_TRIFORCE" \
		or system.getSubNavigation == "all_CHIHIRO" or system.getSubNavigation == 'Favorite' or system.sub_display_genre == True:
			if system.getDisplayNextPrev:
				#Prev Button
				if 5 <= touch_pos[0] <= 45 and 140 <= touch_pos[1] <= 180:
					system.decrIndexGame
					
				#Next Button	
				if 435 <= touch_pos[0] <= 475 and 140 <= touch_pos[1] <= 180:
					system.incrIndexGame
					
		#Prev Next Games	
		if system.getSubNavigation == "genre_ATOMISWARE" or system.getSubNavigation == "genre_NAOMI" \
		or system.getSubNavigation == "genre_NAOMI2" or system.getSubNavigation == "genre_TRIFORCE" \
		or system.getSubNavigation == "genre_CHIHIRO" or system.getSubNavigation == 'Favorite':
			if system.getDisplayNextPrev and system.sub_display_genre == False:
				#Prev Button
				if 5 <= touch_pos[0] <= 45 and 140 <= touch_pos[1] <= 180:
					system.decrIndexGenre
					
				#Next Button	
				if 435 <= touch_pos[0] <= 475 and 140 <= touch_pos[1] <= 180:
					system.incrIndexGenre
					
					
					
		#Prev Next Gallery
		if system.display_gallery:
			#Prev Button
			if 5 <= touch_pos[0] <= 45 and 140 <= touch_pos[1] <= 180:
				system.decrIndexGallery
				
			#Next Button	
			if 435 <= touch_pos[0] <= 475 and 140 <= touch_pos[1] <= 180:
				system.incrIndexGallery			
				
				
				
				
		return navigation
				


		
		
	def interactionIpAdress(self, setting, touch_pos, system):
		#Button Ping
		if 15 <= touch_pos[0] <= 134 and 263 <= touch_pos[1] <= 305:
			
			setting.setFalsePingTestResult
			setting.pingIdAddress
		
		#Button Set New IP
		if 325 <= touch_pos[0] <= 442 and 263 <= touch_pos[1] <= 305:
			setting.setFalsePingTestResult
			setting.setNewIpAddress(system)
		
		#Number IP 0
		if 25 <= touch_pos[0] <= 50 and 120 <= touch_pos[1] <= 140:
			setting.setFalsePingTestResult
			setting.setChangeValue(True)
			setting.ip0 = setting.rotateIncrNumber(int(setting.ip0))
		
		if 25 <= touch_pos[0] <= 50 and 200 <= touch_pos[1] <= 220:
			setting.setFalsePingTestResult
			setting.setChangeValue(True)
			setting.ip0 = setting.rotateDecrNumber(int(setting.ip0))
			
		#Number IP 1	
		if 60 <= touch_pos[0] <= 86 and 120 <= touch_pos[1] <= 140:
			setting.setFalsePingTestResult
			setting.setChangeValue(True)
			setting.ip1 = setting.rotateIncrNumber(int(setting.ip1))
		
		if 60 <= touch_pos[0] <= 86 and 200 <= touch_pos[1] <= 220:
			setting.setFalsePingTestResult
			setting.setChangeValue(True)
			setting.ip1 = setting.rotateDecrNumber(int(setting.ip1))
			
		#Number IP 2
		if 96 <= touch_pos[0] <= 123 and 120 <= touch_pos[1] <= 140:
			setting.setFalsePingTestResult
			setting.setChangeValue(True)
			setting.ip2 = setting.rotateIncrNumber(int(setting.ip2))
		
		if 96 <= touch_pos[0] <= 123 and 200 <= touch_pos[1] <= 220:
			setting.setFalsePingTestResult
			setting.setChangeValue(True)
			setting.ip2 = setting.rotateDecrNumber(int(setting.ip2))
			
		#Number IP 3
		if 144 <= touch_pos[0] <= 172 and 120 <= touch_pos[1] <= 140:
			setting.setFalsePingTestResult
			setting.setChangeValue(True)
			setting.ip3 = setting.rotateIncrNumber(int(setting.ip3))
		
		if 144 <= touch_pos[0] <= 172 and 200 <= touch_pos[1] <= 220:
			setting.setFalsePingTestResult
			setting.setChangeValue(True)
			setting.ip3 = setting.rotateDecrNumber(int(setting.ip3))
			
		#Number IP 4
		if 180 <= touch_pos[0] <= 205 and 120 <= touch_pos[1] <= 140:
			setting.setFalsePingTestResult
			setting.setChangeValue(True)
			setting.ip4 = setting.rotateIncrNumber(int(setting.ip4))
		
		if 180 <= touch_pos[0] <= 205 and 200 <= touch_pos[1] <= 220:
			setting.setFalsePingTestResult
			setting.setChangeValue(True)
			setting.ip4 = setting.rotateDecrNumber(int(setting.ip4))
			
		#Number IP 5	
		if 212 <= touch_pos[0] <= 240 and 120 <= touch_pos[1] <= 140:
			setting.setFalsePingTestResult
			setting.setChangeValue(True)
			setting.ip5 = setting.rotateIncrNumber(int(setting.ip5))
		
		if 212 <= touch_pos[0] <= 240 and 200 <= touch_pos[1] <= 220:
			setting.setFalsePingTestResult
			setting.setChangeValue(True)
			setting.ip5 = setting.rotateDecrNumber(int(setting.ip5))
			
		#Number IP 6
		if 255 <= touch_pos[0] <= 280 and 120 <= touch_pos[1] <= 140:
			setting.setFalsePingTestResult
			setting.setChangeValue(True)
			setting.ip6 = setting.rotateIncrNumber(int(setting.ip6))
		
		if 255 <= touch_pos[0] <= 280 and 200 <= touch_pos[1] <= 220:
			setting.setFalsePingTestResult
			setting.setChangeValue(True)
			setting.ip6 = setting.rotateDecrNumber(int(setting.ip6))
			
		#Number IP 7
		if 288 <= touch_pos[0] <= 316 and 120 <= touch_pos[1] <= 140:
			setting.setFalsePingTestResult
			setting.setChangeValue(True)
			setting.ip7 = setting.rotateIncrNumber(int(setting.ip7))
		
		if 288 <= touch_pos[0] <= 316 and 200 <= touch_pos[1] <= 220:
			setting.setFalsePingTestResult
			setting.setChangeValue(True)
			setting.ip7 = setting.rotateDecrNumber(int(setting.ip7))
			
		#Number IP 8
		if 322 <= touch_pos[0] <= 348 and 120 <= touch_pos[1] <= 140:
			setting.setFalsePingTestResult
			setting.setChangeValue(True)
			setting.ip8 = setting.rotateIncrNumber(int(setting.ip8))
		
		if 322 <= touch_pos[0] <= 348 and 200 <= touch_pos[1] <= 220:
			setting.setFalsePingTestResult
			setting.setChangeValue(True)
			setting.ip8 = setting.rotateDecrNumber(int(setting.ip8))
			
		#Number IP 9	
		if 366 <= touch_pos[0] <= 390 and 120 <= touch_pos[1] <= 140:
			setting.setFalsePingTestResult
			setting.setChangeValue(True)
			setting.ip9 = setting.rotateIncrNumber(int(setting.ip9))
		
		if 366 <= touch_pos[0] <= 390 and 200 <= touch_pos[1] <= 220:
			setting.setFalsePingTestResult
			setting.setChangeValue(True)
			setting.ip9 = setting.rotateDecrNumber(int(setting.ip9))
			
		#Number IP 10
		if 397 <= touch_pos[0] <= 422 and 120 <= touch_pos[1] <= 140:
			setting.setFalsePingTestResult
			setting.setChangeValue(True)
			setting.ip10 = setting.rotateIncrNumber(int(setting.ip10))
		
		if 397 <= touch_pos[0] <= 422 and 200 <= touch_pos[1] <= 220:
			setting.setFalsePingTestResult
			setting.setChangeValue(True)
			setting.ip10 = setting.rotateDecrNumber(int(setting.ip10))
			
		#Number IP 11
		if 433 <= touch_pos[0] <= 460 and 120 <= touch_pos[1] <= 140:
			setting.setFalsePingTestResult
			setting.setChangeValue(True)
			setting.ip11 = setting.rotateIncrNumber(int(setting.ip11))
		
		if 433 <= touch_pos[0] <= 460 and 200 <= touch_pos[1] <= 220:
			setting.setFalsePingTestResult
			setting.setChangeValue(True)
			setting.ip11 = setting.rotateDecrNumber(int(setting.ip11))
			
			
	