PiDimBoot
============

PiDimBoot is based on PiforceTools (<https://github.com/travistyoj/piforcetools>) for load games to Sega Netdimm (Naomi 1 and 2, TriForce, Chihiro) with Raspberry and Lcd resistive touch screen 3.5" (320x480).

My code source is 100% FREE, OPEN SOURCE, and FORKABLE :)
Thanks no business, no money for this application, it's only FREE

![PiDimBoot Screenshot](./img_src_readme/main.jpg)

## Usage

1. List Games Compatibles : <https://docs.google.com/spreadsheets/d/12xo7xKHE54Zm7QdquY0h5M3HkVmz6H8Dt4crrbhjORk/>
2. Startup PiDimBoot : <https://youtu.be/7DgBk39FfiE>
3. Set Virtual Zero Key Chip : <https://youtu.be/M7UVBQ0CILQ>
4. Set IP Address by System : <https://youtu.be/dNdGQEcgTYQ>
5. Navigate to PiDimBoot. By System, By Genre : <https://youtu.be/xq9jMqT8gOU>
6. Launch Game : <https://youtu.be/vflhOEDOe5Q>
7. Favorite Add/Delete/Display Game : <https://youtu.be/9S-s0mjNalg>
8. Launch Autoboot One Game : <https://youtu.be/cApJJM8fajM>
9. Power OFF Raspberry and PiDimBoot : <https://youtu.be/XCpo7qEe0-U>

## Getting Started

You will need the following items to use PiDimTouch:

1. A Raspberry Pi 3 - <http://www.raspberrypi.org/>
2. An SD Card (Minimum 4GB, but I recommend at 8GB or 64GB for fullset games)
3. LCD Resistives Touch Screen 320x480 - (example : <https://www.amazon.fr/gp/product/B07NTH1JWH/ref=ppx_yo_dt_b_asin_image_o00_s00?ie=UTF8&psc=1>
4. A Naomi, Triforce, or Chihiro arcade system.
5. A Netdimm with a zero-key security PIC installed.  I cannot provide links for this, but a modicum of Google-fu will get you what you need.  The netdimm will need to be configured.
Example:

![PiDimTouch Screenshot](./img_src_readme/zeroKey0.jpg)
![PiDimTouch Screenshot](./img_src_readme/zeroKey1.jpg)

6. A crossover cable

## Installation
#### Easy install with img
Now you are finally ready to install PiDimBoot.

1. Downlad the PiDimBoot SD card image:  <https://drive.google.com/file/d/1hXcbC8t2nsg2IgXFzPsesb7M2VLCk2Zp/view?usp=sharing>
2. Extract .img file, and use imager tool to write it to your SD card.  If you are using Windows, look for Win32DiskImager.  If you are using Linux or Mac OS, you will use BalenaEtcher <https://www.balena.io/etcher/>
3. Use a partition manager tool like Partition Wizard to move the Ext4 partition to the end of the card, and resize the FAT partition to use all unallocated space: http://www.partitionwizard.com/free-partition-manager.html
4. Load up ROMs in the "/boot/pidimboot/roms" directory.


#### Manually
* Download and install last Raspbian Lite <https://www.raspberrypi.org/downloads/raspbian/>
* Remove functionality not util
```
sudo apt-get remove bluez bluez-firmware pi-bluetooth triggerhappy pigpio
```
* Install Git
```
sudo apt-get install git
```

* Installation Drivers TFT, go to website Manufacturer and install your Driver

```
Good Luck, Personnaly for my Chinese TouchScreen
SSH only, no HDMI screen connected

sudo su
git clone https://github.com/goodtft/LCD-show.git
chmod -R 755 LCD-show
cd LCD-show/
git branch (for certify in Master Branch)
./MHS35-show (and pray...)
```

After auto-reboot, if your touchscreen don't propose normal login,
flash your sd-card and restart operation... Sorry, Chinese Drivers....

Reboot

* Create udev rules (Example : For my screen, Adafruit is different value)
```
sudo nano /etc/udev/rules.d/29-ads7846.rules
```
add
```
SUBSYSTEM=="input", KERNEL=="event[0-9]*", ATTRS{name}=="ADS7846 Touchscreen", SYMLINK+="input/touchscreen"
```

Check /dev/input/ exist input "touchscreen"

*  Installation StartX
```
sudo apt-get --no-install-recommends install xserver-xorg xserver-xorg-video-fbdev xinit pciutils xinput xfonts-100dpi xfonts-75dpi xfonts-scalable
```

* Install dependance PIP for Python
```
sudo apt-get install python-pip git libsdl-dev libsdl-image1.2-dev libsdl-mixer1.2-dev libsdl-ttf2.0-dev libsmpeg-dev libportmidi-dev libavformat-dev libswscale-dev libjpeg-dev libfreetype6-dev evtest tslib libts-bin xinput libts-bin
```

* Pre-Calibrate Touchscreen
```
export FRAMEBUFFER=/dev/fb1 
startx
```

* Calibrate Touchscreen

		sudo TSLIB_FBDEVICE=/dev/fb1 TSLIB_TSDEVICE=/dev/input/touchscreen ts_calibrate
		sudo TSLIB_FBDEVICE=/dev/fb1 TSLIB_TSDEVICE=/dev/input/touchscreen ts_test
		
* Installation Pygame

```
sudo apt-get update
sudo apt-get install python-pygame
```
		
* Log in Ram

```
sudo nano /etc/fstab

tmpfs /tmp tmpfs defaults,noatime,nosuid,size=10m 0 0
tmpfs /var/tmp tmpfs defaults,noatime,nosuid,size=10m 0 0
tmpfs /var/log tmpfs defaults,noatime,nosuid,mode=0755,size=10m 0 0
```

* Installation Pidimboot

```
sudo mkdir /boot/pidimboot /boot/pidimboot/roms

Create empty file
sudo nano /boot/pidimboot/autoboot.txt

cd /opt/

git clone https://gitlab.com/ezechiel/pidimboot.git
```

* Downgrading to sdl 1.2.15-5

		echo "deb http://archive.raspbian.org/raspbian wheezy main" > /etc/apt/sources.list.d/wheezy.list

		echo "APT::Default-release \"stable\";" > /etc/apt/apt.conf.d/10defaultRelease

		echo "Package: libsdl1.2debian Pin: release n=jessie Pin-Priority: -10 Package: libsdl1.2debian Pin: release n=wheezy Pin-Priority: 900" > /etc/apt/preferences.d/libsdl

		apt-get update
		apt-get -y --force-yes install libsdl1.2debian/wheezy
		
If archive.raspbian.org respond 404... Don't Panic. Install libsdl1.2debian into DIR SDLLibDebian

```
cd /opt/pidimboot/SDLLibDebian
dpkg -i libsdl1.2debian_1.2.15-5+rpi1_armhf.deb
```

And check value
```
dpkg -l | grep sdl
```
Result :
```
libsdl1.2debian:armhf           1.2.15-5+rpi1                     armhf        Simple DirectMedia Layer
```

Great Job :)

* Launch automatically Application PiDimBooot

```
sudo nano ./bashrc

scroll endfile and add
if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
	echo "Welcome SSH"
else
  sudo python /opt/pidimboot/main.py
fi
```

* Set Ip static

```
sudo nano /etc/dhcpcd.conf

Scrool and Set (view picture)
#interface eth0
static ip_address=192.168.1.99
static routers=192.168.1.1
```

Finish :)


## Config IP Netdimm
![PiDimTouch Screenshot](./img_src_readme/setIP0.jpg)
![PiDimTouch Screenshot](./img_src_readme/setIP1.jpg)
![PiDimTouch Screenshot](./img_src_readme/setIP2.jpg)
![PiDimTouch Screenshot](./img_src_readme/setIP3.jpg)



## Credit
Thank triforcetools and travistyoj for PiforceTools.
Thank darksoft for killer Atomiswave conversions.
Thank Mr_lune (NeoArcadia) for Covers Picture artset game.
Thank Aetios, Squallrs et Pourq (NeoArcadia) for design and create STL Naomi Cabinet Integration.